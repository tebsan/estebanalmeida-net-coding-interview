﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController : SecureFlightBaseController
{
    private readonly IRepository<Flight> _flightRepository;
    private readonly IRepository<PassengerFlight> _passengerFlightRepository;
    private readonly IService<Flight> _flightService;

    public FlightsController(IService<Flight> flightService, IRepository<Flight> flightRepository, 
        IRepository<PassengerFlight> passengerFlightRepository, IMapper mapper)
        : base(mapper)
    {
        _flightService = flightService;
        _flightRepository = flightRepository;
        _passengerFlightRepository= passengerFlightRepository;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await _flightService.GetAllAsync();
        return GetResult<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpPost]
    [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public IActionResult Post(long flightId, string passengerId)
    {
        var result = _passengerFlightRepository.Insert(new PassengerFlight
        {
            FlightId = flightId,
            PassengerId = passengerId
        });

        return Ok(result);
    }
}